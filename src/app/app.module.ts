import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Card } from './components/card/card.component';
import { HttpClientModule } from '@angular/common/http';
import { NumberRandom } from './util/numberRandom';

@NgModule({
  declarations: [
    AppComponent,
    Card
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    MatRadioModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [NumberRandom],
  bootstrap: [AppComponent]
})
export class AppModule { }
