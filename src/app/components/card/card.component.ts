import { ICharacter } from './../../interface/ICharacter';
import { Component, OnInit } from "@angular/core";
import { StarWarsService } from '../../service/star-wars.service';
import { NumberRandom} from '../../util/numberRandom';

@Component({
    selector:'app-card',
    templateUrl:'./card.component.html',
    styleUrls:['./card.component.css']
})
export class Card implements OnInit{
  charactersResponse: ICharacter [] = [];
  winnerCharacterMatch !: ICharacter;
  winnerCharacter: string = "";
  msg_error: string ="";
  indexFirstCharacter : number =0;
  indexSecondCharacter : number =0;
  isSubmitted = false;
  attributePower: string = "";
  powers: string[] = ["Height","Mass", "Birth Year", "Number of Films"]
  constructor(private service: StarWarsService, private numeroRandom: NumberRandom){

  }
  ngOnInit(){
    this.service.getCharacters().subscribe(response => {

      this.indexFirstCharacter = this.numeroRandom.getNumberRandom(response.count);
      this.indexSecondCharacter = this.numeroRandom.getNumberRandom(response.count);
      while(this.indexFirstCharacter === this.indexSecondCharacter){
        this.indexSecondCharacter = this.numeroRandom.getNumberRandom(response.count);
      }

      this.service.getCharacterId(this.indexFirstCharacter).subscribe(response=>{
        console.log(response);
        this.charactersResponse.push(response);
      }, error => {
        console.log("Get Character by Id error!!!", error);
      })

      this.service.getCharacterId(this.indexSecondCharacter).subscribe(response=>{
        console.log(response);
        this.charactersResponse.push(response);
      }, error => {
        console.log("Get Character by Id error!!! ", error);
      })


    }, error=>{
      console.log("Error: ", error);
    });
  }

  getBattle():void {
    switch(this.attributePower){
      case 'Height':
        this.msg_error = "";
        if(parseInt(this.charactersResponse[0].height) > parseInt(this.charactersResponse[1].height)){
          this.winnerCharacter = this.charactersResponse[0].name;
        }else{
          this.winnerCharacter = this.charactersResponse[1].name;
        }
        break;
      case 'Mass':
        this.msg_error = "";
        if(parseInt(this.charactersResponse[0].mass) > parseInt(this.charactersResponse[1].mass)){
          this.winnerCharacter = this.charactersResponse[0].name;
        }else{
          this.winnerCharacter = this.charactersResponse[1].name;
        }
        break;
      case 'Number of Films':
        this.msg_error = "";
        if(parseInt(this.charactersResponse[0].films.length) > parseInt(this.charactersResponse[1].films.length)){
          this.winnerCharacter = this.charactersResponse[0].name;
        }else{
          this.winnerCharacter = this.charactersResponse[1].name;
        }
        break;
      case 'Birth Year':
        this.msg_error = "";
        if(parseInt(this.charactersResponse[0].birth_year) > parseInt(this.charactersResponse[1].birth_year)){
          this.winnerCharacter = this.charactersResponse[0].name;
        }else{
          this.winnerCharacter = this.charactersResponse[1].name;
        }
        break;
      default:
        this.msg_error = "Please, select one of options above!";
        break;
    }
  }

  reloadCurrentPage() {
    window.location.reload();
  }
}
