export interface ICharacter{
  name: string,
  birth_year: string,
  height: string,
  mass: string,
  films: any
}
