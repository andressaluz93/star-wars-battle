import { ICharacter } from './../interface/ICharacter';
import { API_PATH } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StarWarsService {

  constructor(private httpClient: HttpClient) { }

  getCharacters(): Observable <any> {
    return this.httpClient.get<any>(`${API_PATH}people`);
  }

  getCharacterId(id: number): Observable<ICharacter>{
    return this.httpClient.get<ICharacter>(`${API_PATH}people/${id}`);
  }
}
